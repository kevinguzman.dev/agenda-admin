<?php
#--- imprime arreglo en formato legible
function pre($arreglo, $detener){
  echo "<pre>";
  print_r($arreglo);
  echo "</pre>";

  if($detener):
    exit;
  endif;
}

#--- retorna numero con formato miles
function miles($numero){
  return number_format($numero,  0, '', '.');
}

#--- retorna numero con formato dinero
function dinero($numero){
  if(is_numeric($numero)):
    if($numero != ""):
      return "$ ".number_format($numero,  0, '', '.');
    else:
      return "";
    endif;
  else:
    return "";
  endif;
}

#--- quita el formato dinero
function no_dinero($numero){
  $numero2 = str_replace($numero, ".", "");
  return $numero2;
}

#--- retorna fecha en formato [dia]-[mes]-[año] [hora]:[minutos]:[segundos]
function dt($fecha){
  $fecha=date_create($fecha);
  return date_format($fecha,"d-m-Y H:i:s");
}

function d($fecha){
  $fecha=date_create($fecha);
  return date_format($fecha,"d-m-Y");
}

#--- retorna fecha en formato Y-m-d
function dbd($fecha){
  $fecha = date("Y-m-d", strtotime($fecha));
  return $fecha;
}

#--- retorna el estado de un periodo en palabras
function estadoPeriodo($per){
  switch($per):
    case 'A':
      return "Abierto";
    case 'C':
      return "Cerrado";
  endswitch;
}

#--- retorna array con los meses del año
function mes(){
  #--- array con los meses del año
  $Gd_meses     = array();
  $Gd_meses[]   = "Enero";
  $Gd_meses[]   = "Febrero";
  $Gd_meses[]   = "Marzo";
  $Gd_meses[]   = "Abril";
  $Gd_meses[]   = "Mayo";
  $Gd_meses[]   = "Junio";
  $Gd_meses[]   = "Julio";
  $Gd_meses[]   = "Agosto";
  $Gd_meses[]   = "Septiembre";
  $Gd_meses[]   = "Octubre";
  $Gd_meses[]   = "Noviembre";
  $Gd_meses[]   = "Diciembre";

  return $Gd_meses;
}

function mesId($id){
  return mes()[$id-1];
}

#--- retorna el nombre sugerido para el periodo
function nombrePeriodo(){
  #--- array con los meses del año
  $Gd_meses     = array();
  $Gd_meses[]   = "Enero";
  $Gd_meses[]   = "Febrero";
  $Gd_meses[]   = "Marzo";
  $Gd_meses[]   = "Abril";
  $Gd_meses[]   = "Mayo";
  $Gd_meses[]   = "Junio";
  $Gd_meses[]   = "Julio";
  $Gd_meses[]   = "Agosto";
  $Gd_meses[]   = "Septiembre";
  $Gd_meses[]   = "Octubre";
  $Gd_meses[]   = "Noviembre";
  $Gd_meses[]   = "Diciembre";

  $Gd_mesActual = $Gd_meses[date('m')-1];
  $Gd_anoActual = date('Y');
  $Gd_return    = $Gd_mesActual."-".$Gd_anoActual;
  return $Gd_return;
}

#--- trae la foto de perfil del usuario en sesión
function traerFotoUsuario($url, $user){

  $conn        = new connbd();
  $strconn     = $conn->connect();

  $Gd_foto     = $url."/mods/img/usuarios/";

  $sql  = "SELECT foto FROM usuarios where login = '".$user."'";
  $res  = $strconn->query($sql) or die ("Error: ".mysqli_error($strconn));

  if($res->num_rows > 0):
    $row = $res->fetch_assoc();
    $Gd_foto .= $row["foto"];
  endif;
  $strconn->close();

  return $Gd_foto;
}

#--- sacar miles
function limpiarNumero($num){
  $num = str_replace(".", "", $num);
  $num = str_replace(",", "", $num);
  return $num;
}

#--- trae estado del usuario según su sigla
function traerEstadoUsuario($estado){
  switch ($estado) {
    case 'A':
      return "Activo";
    case 'I':
      return "Inactivo";
    default:
      return "";
  }
}

function Hoy(){
  return date('d-m-Y');
}

#--- tran
function FechaBD($Gd_fecha){
  $Gd_fecha = strtr($Gd_fecha, '/', '-');
  $Gd_fecha = date_create($Gd_fecha);
  return date_format($Gd_fecha,"Y-m-d");
}

function BDFecha($Gd_fecha){
  $Gd_fecha = date_create($Gd_fecha);
  return date_format($Gd_fecha,"d-m-Y");
}

#--- limpia la variable
function clear($variable, $mysqli){
  $ret = "";
  $ret = $mysqli->real_escape_string($variable);
  return $ret;
}

function getUser($id){
  $conn        = new connbd();
  $strconn     = $conn->connect();
  $id          = clear($id, $strconn);

  $sql         = "select nombre from usuarios where codusuario = ".$id." limit 1";
  $res         = $strconn->query($sql) or die ("Error getUser: " . mysqli_error($strconn));

  if($res->num_rows > 0):
    $row = $res->fetch_array();
    $strconn->close();
    return $row["nombre"];
  endif;
}

function ValidarAccesoModulo($id_perfil, $modulo){
  $conn        = new connbd();
  $strconn     = $conn->connect();
  $id_perfil   = clear($id_perfil, $strconn);
  $modulo      = clear($modulo, $strconn);

  $sql         = "SELECT count(id_perfil) as count FROM perfil_modulos WHERE id_perfil = ".$id_perfil." AND id_modulo = ".$modulo;
  $res         = $strconn->query($sql) or die("error validar perfil: " . mysqli_error($strconn));
  $valida      = false;

  if($res->num_rows > 0): 
    $row = $res->fetch_assoc();
    if($row["count"] > 0):
      $valida = true;
    endif;
  endif;

  $strconn->close();
  return $valida;
}

function EstructuraHtml()
{
	$html = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
				    <meta name="viewport" content="width=device-width, initial-scale=1.0">				
					<title>Notificación de Correo Electrónico</title>
				</head>				
				<body>
					<div style="font-family:Arial, Helvetica, sans-serif; background-color:#FAFAFA; width:90%; min-height:520px; padding-top: 20px; padding-left: 5%; padding-bottom: 20px; padding-right: 5%">
					    <div style="margin:0 auto; background-color:#ffffff; color:#505050; font-size:14px; max-width:570px; min-height:400px; font-family: Helvetica; line-height:150%; text-align:left; border:#E1E1E1 solid 1px">
						    <div style="background-color:#F8F8F8; max-width:570px; height:85px; border-bottom:#E1E1E1 solid 1px">
							    <!--<img src="css/images/logo-email.png" alt="" style="margin-top:20px; margin-left:20px "/> -->
						    </div>
							<div style="padding:20px;">	
								**CONTENIDO**
							</div>
						    <div style="margin:20px 5px 10px 5px; text-align: center; font-size:11px;">
							    <!-- <a href="#">Visita nuestro sitio web</a> | <a href="#">¿Necesitas Ayuda?</a><br/> -->
							    Copyright © kGym
						    </div>						
					    </div>
					    <br/>
					</div>
				</body>				
			 </html>';
			
	return $html;
}

#--- envía un correo electrónico a través de PHPMailer
function EnviarCorreo($asunto, $correo, $mensaje){
  require 'mailer/Exception.php';
  require 'mailer/PHPMailer.php';
  require 'mailer/SMTP.php';
  
  date_default_timezone_set('America/Santiago');	
                        
  $mail               = new PHPMailer\PHPMailer\PHPMailer(true);
  
  $mail->SMTPDebug    = 0;                                                                      
  $mail->Host         = 'mail.ksolutions.cl';  
  $mail->SMTPAuth     = true;                               
  $mail->Username     = 'cicerocostha@ksolutions.cl';  
  $mail->Password     = 'lwgvsHJc0[Is';                     
  $mail->Port         = 465;  
  $mail->SMTPSecure   = 'ssl';     
  $mail->CharSet      = 'UTF-8';   
  $mail->Encoding     = 'base64';                          

  $mail->isSMTP(); 
  $mail->ClearAddresses();
  $mail->setFrom('cicerocostha@ksolutions.cl', 'Salud y Rendimiento');
  $mail->AddReplyTo( 'cicerocostha@ksolutions.cl', 'Cicero costha' );   
  $mail->isHTML(true);      
  $mail->addAddress($correo);  

  $mail->Subject = html_entity_decode(htmlentities($asunto));

  #--- cuerpo del correo
  $body             = EstructuraHtml();
  $body             = str_replace("[\]",'',$body);
  $body             = str_replace('**CONTENIDO**',nl2br($mensaje),$body);	
  
  $mail->Body    = $body;
  
  if(!$mail->send()){
    echo $mail->ErrorInfo;
  }

}

#--- genera string random
function generaString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
} 

#--- genera nombre de usuario formato primera letra del nombre + apellido
function generaNombreUsuario($nombre, $apellido){
  $conn        = new connbd();
  $strconn     = $conn->connect();
  
  #--- limpia las variables
  $nombre      = clear($nombre, $strconn); 
  $apellido    = clear($apellido, $strconn); 

  #--- transforma a minúsculas las variables
  $nombre      = strtolower($nombre);
  $apellido    = strtolower($apellido);

  #--- primera chance, primera letra + apellido ej: kguzman
  for($i = 1; $i < strlen($nombre); $i++):
    $nombreFinal = substr($nombre, 0, $i);
    $nombreFinal = $nombreFinal.".".$apellido;
  
    #--- preguntamos si existe algún usuario con ese username
    $consulta    = "SELECT codusuario FROM usuarios WHERE login = '$nombreFinal'";
    $respuesta   = $strconn->query($consulta);
    $fila        = $respuesta->fetch_array();

    #--- si no existe, retornará el nombre de usuario
    if(is_null($fila["codusuario"])):
      return $nombreFinal;
    endif;  
  endfor;

  return $nombre.$apellido;
}

function getNombreAlumno($id){
  $nombre       = "";
  $conn         = new connbd();
  $strconn      = $conn->connect();
  
  $query        = "SELECT nombres, appaterno, apmaterno FROM alumno WHERE codalumno = '$id' LIMIT 1";
  $resultado    = $strconn->query($query) or die("error get nombre alumno " . mysqli_error());
  $fila         = $resultado->fetch_assoc();

  $nombre       = $fila["nombres"]." ".$fila["appaterno"]." ".$fila["apmaterno"];
  $strconn->close();

  return $nombre;
}

function getEmailAlumno($id){
  $conn         = new connbd();
  $strconn      = $conn->connect();
  
  $query        = "SELECT email FROM alumno WHERE codalumno = '$id' LIMIT 1";
  $resultado    = $strconn->query($query) or die("error get nombre alumno " . mysqli_error());
  $fila         = $resultado->fetch_assoc();

  $email        = $fila["email"];
  $strconn->close();
  
  return $email;
}


function getPlanesAlumnos($id){
  $array        = array();
  $conn         = new connbd();
  $strconn      = $conn->connect();

  $query        =  "SELECT nombre, valor 
                    FROM planes t1 
                    INNER JOIN alumnos_planes t2 ON t1.id = t2.id_plan
                    WHERE t2.id_alumno = '$id'";
  $resultado    = $strconn->query($query) or die ("Error getPlanesAlumnos: " . mysqli_error($strconn));

  while($row = $resultado->fetch_assoc()){
    $obj          = new stdClass();
    $obj->nombre  = $row["nombre"];
    $obj->valor   = dinero($row["valor"]);

    $array[]      = $obj;
  }

  $strconn->close();
  return $array;

}
?>
