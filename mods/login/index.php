<?php
session_start();

if(isset($_GET["id"]) and $_GET["id"] == "out"):
  $_SESSION["User"] = null;
  $_SESSION = null;
  session_destroy();
endif;

if(isset($_SESSION["Login"]) && $_SESSION["Login"] != ""){
  header("Location: ../dashboard/index");
}

  #--- llama a funciones
  require_once("../required/functions.php");
  
  #--- leer variables globales
  $Gl_appName   = "";
  $Gl_appUrl    = "";

  $Gd_json      = json_decode(file_get_contents("../required/config.json"));
  $Gl_appName   = $Gd_json->{"appName"};
  $Gl_appUrl    = $Gd_json->{"appUrl"};

  require_once("../required/connbd.php");
  $Gd_user      = "";
  $Gd_password  = "";
  $Gd_error     = "";

  if( isset($_POST["Username"]) && isset($_POST["Contrasena"]) ):
    $Gd_user      = $_POST["Username"];
    $Gd_password  = $_POST["Contrasena"];

    require_once("login.php");
    $log  = new Login();
    $ret  = $log->Login($Gd_user, $Gd_password);

    if($ret["user"] != "" && $ret["username"]):
      session_start();
      
      $_SESSION["User"]         = $ret["username"];
      $_SESSION["Login"]        = $ret["user"];
      $_SESSION["periodo"]      = $ret["periodo"];
      $_SESSION["nomperiodo"]   = $ret["nomperiodo"];
      $_SESSION["UserId"]       = $ret["userId"];
      $_SESSION["Perfil"]       = $ret["perfil"];
      $_SESSION["NomPerfil"]    = $ret["nPerfil"];
      $_SESSION["estado"]       = $ret["estado"];

      $Gd_mesPaso   = date("m, Y", strtotime($ret["userDate"]));
      $Gd_mesPaso2  = explode(",", $Gd_mesPaso);
      $Gd_mes       = $Gd_mesPaso2[0];
      $Gd_anno      = $Gd_mesPaso2[1];
      $Gd_mes       = mesId($Gd_mes);

      $_SESSION["userDate"]     = $Gd_mes." del ".$Gd_anno;
      
      switch ($ret["estado"]) :
        case "R": #--- cuando cambió la contraseña
          header("Location: ../recuperar-contrasenna/form");
          break;
        case "A": #--- cuando ingresa correctamente al sistema
          header("Location: ../dashboard/index");
          break;
        default:
          $Gd_error = "Usuario y/o contraseña incorrectas";
          break;
      endswitch;

    else:
      $Gd_error = "Usuario y/o contraseña incorrectas";
    endif;
  endif;
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>kGym | Log in</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/assets/plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/assets/plugins/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=$Gl_appUrl ?>/assets/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
  body{
    background-image: url("../img/sys/bg.jpg");
  }
</style>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#"><b>k</b>GYM </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <?php if($Gd_error != ""): ?>
      <div class="alert alert-danger fadeIn">
        <?= $Gd_error ?>
      </div>
    <?php endif; ?>
      <p class="login-box-msg">Ingresa tus datos para empezar...</p>

      <form action="<?=$Gl_appUrl ?>/login/index" method="post">
        <div class="form-group has-feedback">
          <input type="input" class="form-control" placeholder="Usuario" name="Username" id="Username" autocomplete="off" autofocus>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Contraseña" name="Contrasena" id="Contrasena">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
          </div>
        </div>
      </form>

      <div class="social-auth-links text-center">
        <a href="<?= $Gd_json->{"appUrl"} ?>/recuperar-contrasenna/index">Olvidé mi contraseña</a><br>
      </div>
    </div>
    <!-- /.login-box-body -->
  </div>

  <script src="<?=$Gl_appUrl ?>/assets/plugins/jquery/dist/jquery.min.js"></script>
  <script src="<?=$Gl_appUrl ?>/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
