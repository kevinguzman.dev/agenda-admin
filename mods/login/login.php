<?php
class Login{
  function __construct(){}

  function Login($user, $password){
    $conn        = new connbd();
    $strconn     = $conn->connect();

    $user       = clear($user, $strconn);
    $password   = clear($password, $strconn);

    $sql        = "SELECT t1.nombre, t1.login, t1.creacion, codusuario, id_perfil, t2.nombre as nomperfil, t1.estado ";
    $sql       .= "FROM usuarios t1 ";
    $sql       .= "INNER JOIN perfil t2 ON t1.id_perfil = t2.id ";
    $sql       .= "where t1.login = '". $user . "' and password = '" . $password . "' and t2.estado = 'A' ";
    
    $resultado  = $strconn->query($sql);

    $Gd_userNick    = "";
    $Gd_userName    = "";
    $Gd_userDate    = "";
    $Gd_periodo     = "";
    $Gd_nomPeriodo  = "";
    $Gd_userId      = "";
    $Gd_perfil      = "";
    $Gd_nPerfil     = "";
    $Gd_estado      = "";

    if($resultado->num_rows > 0):
      $row          = $resultado->fetch_assoc();
      $Gd_userNick  = $row["login"];
      $Gd_userName  = $row["nombre"];
      $Gd_userDate  = $row["creacion"];
      $Gd_userId    = $row["codusuario"];
      $Gd_perfil    = $row["id_perfil"];
      $Gd_nPerfil   = $row["nomperfil"];
      $Gd_estado    = $row["estado"];

    endif;

    $sql  = "SELECT codperiodo, nombre FROM periodos WHERE estado = 'A' LIMIT 1";
    $res  = $strconn->query($sql) or die("Error periodo: " . mysqli_error($strconn));

    if($res->num_rows > 0):
      $row = $res->fetch_assoc();
      $Gd_periodo     = $row["codperiodo"];
      $Gd_nomPeriodo  = $row["nombre"];
    endif;

    $strconn->close();

    $ret = array(
                  "user"        => $Gd_userNick,
                  "username"    => $Gd_userName,
                  "userDate"    => $Gd_userDate,
                  "userId"      => $Gd_userId,
                  "periodo"     => $Gd_periodo,
                  "nomperiodo"  => $Gd_nomPeriodo,
                  "perfil"      => $Gd_perfil,
                  "nPerfil"     => $Gd_nPerfil,
                  "estado"      => $Gd_estado
                );
    return $ret;
  }
}
?>
