<?php
  require_once("../required/header.php");
  require_once("usuario.php");

  $obj      = new Usuario();
  $Gd_users = json_encode($obj->GetAll());
?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Usuarios
      <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= $Gl_appUrl ?>/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Usuarios</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Listado de todos los usuarios registrados</h3>
        <div class="box-tools">
          <a href="<?= $Gl_appUrl ?>/usuarios/form" class="btn btn-default">Agregar nuevo</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="alumnos" class="table table-striped responsive table-hover">
          <thead>
            <th>Creación</th>
            <th>Nombre</th>
            <th>Login</th>
            <th>Foto</th>
            <th>Perfil</th>
            <th>Estado</th>
            <th>Acción</th>
          </thead>
        </table>
      </div>
    </div>
  </section>

<?php require_once("../required/footer.php");?>
<script type="text/javascript">
$(function () {
  $('#alumnos').DataTable({
    'language'      : { "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json" },
    'paging'        : true,
    'lengthChange'  : true,
    'searching'     : true,
    'ordering'      : true,
    'info'          : true,
    'autoWidth'     : false,
    'responsive'    : true,
    'data'          : <?= $Gd_users ?>,
    'columns'       : [
                        { data: "creacion" },
                        { data: "nombre" },
                        { data: "login" },
                        {
                          sortable: false,
                          className: "table-view-pf-actions",
                          "render": function (data, type, row, meta) {
                            return "<img src='<?=$Gl_appUrl?>/mods/img/usuarios/" + row.foto + "' class='img img-responsive img-circle' height='50px' width='50px'/>";
                          }
                        },
                        { data: "perfil"},
                        { 
                          sortable: false,
                          "render": function(data, type, row, meta){
                            var est = "";
                            if(row.estado == "A"){
                              est = "Activo";
                              lnk = "cambiarEstado(".concat("'", row.estado, "',", row.codusuario, ")");

                              return "<a href='#' onclick=" + lnk +"><span class='badge bg-green'>"+ est +"</span></a>";
                            }else{
                              est = "Inactivo";
                              lnk = "cambiarEstado(".concat("'", row.estado, "',", row.codusuario, ")");

                              return "<a href='#' onclick=" + lnk +"><span class='badge bg-red'>"+ est +"</span></a>";
                            }

                          }
                        },
                        {
                            sortable: false,
                            className: "table-view-pf-actions",
                            "render": function (data, type, row, meta) {
                              var url = "<a href='<?=$Gl_appUrl?>/usuarios/form/"+ row.codusuario +"' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>";
                              return url;
                            }
                        },
                      ]
  });

  cambiarEstado = function(estado, id){
    AlertConfirm("", "Desea cambiar el estado de este usuario?", function(res){
        if(res){
            var json = new Object();
            json["accion"]  = "EST";
            json["estado"]  = estado;
            json["id"]      = id;

            $.ajax({
                type: "POST",
                url: "<?= $Gl_appUrl ?>/usuarios/ajax",
                data: json,
                success: function(msj){
                    console.log(msj);
                    AlertSuccess("Éxito", "Estado cambiado con éxito", "<?= $Gl_appUrl?>/usuarios/index");
                }
            });
        }
    }, 'warning');
  }
})
</script>
<?php require_once("../required/scripts.php"); ?>
